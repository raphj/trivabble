#!/usr/bin/awk -f

BEGIN {
        bag = table = values = ""
}

{
        table = table "  " $0 "\n"
}

$1 == "-" { sub(/- /, "") }
/^[0-9]/ {
        points = $1
        sub(/.*:/, "")
        gsub(/[×,]/, "")
        for (i = 1; i<= NF; i+=2) {
                lettre = $(i)
                if ((lettre == "blank") || (lettre ~ /[Jj]oker/)) {
                        lettre = " "
                }
                nombre = $(i+1)
                bag = bag "       "
                for (j = 0; j < nombre; j++) {
                         bag = bag " \"" lettre "\","
                }
                bag = bag "\n"
                values = values "        \"" lettre "\": " points ",\n"
        }
}

END {
        sub(/..$/, "\n", bag)
        sub(/..$/, "\n", values)

        print "{\n    \"code\": \"@CODE@\",\n    \"name\": \"@LANG@\",\n"
        #print " /*\n" table "  */\n"
        print "    \"bag\": [\n" bag "    ],\n"
        print "    \"letterValues\": {\n" values "    }"
        print "}"
}
