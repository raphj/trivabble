# -*- Makefile -*-

PORT?=3000

ESLINT?=eslint

TIDY?=tidy

ifeq (, $(shell which $(firstword ${ESLINT})))
	ESLINT?=npx eslint
endif

.PHONY: all help lang start-dev-server

all: lang emptydictlist

help:
	@echo make extract-lang-dists: extract the distributions per language from Wikipedia
	@echo make lang: build the translation files
	@echo make eslint: use ESLint to check conformance against the coding style
	@echo make start-dev-server: start a development server

public/l10n/js/fr.js: $(wildcard l10n/po/*/*.po)
	cd l10n; make

extract-lang-dists:
	cd lang-dists-extractor; make

lang: public/l10n/js/fr.js

emptydictlist: public/dict/list.js

public/dict/list.js:
	mkdir -p public/dict/ && touch public/dict/list.js

eslint:
	-${ESLINT} **/*.js

tidy:
	-${TIDY} -xml -errors -q public/*.html

start-dev-server: lang
	cd server && make start-dev-server
