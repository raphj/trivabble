/*global libD*/

(function (global) {
    "use strict";

    let divAlert;
    let divAlertInput;
    let divAlertConfirm;
    let divAlertButton;
    let alertButtonOK;
    let divAlertCallback;
    let divAlertCallbackYes;
    let divAlertCallbackNo;
    let alertInput;
    let alertSelect;
    let divAlertContent;

    const _ = (window.libD && libD.l10n) ? libD.l10n() : function (s) {
        return s;
    };

    function promptOK() {
        divAlert.style.display = "none";
        if (divAlertCallback) {
            divAlertCallback(alertInput.value);
            divAlertCallback = null;
        }
    }

    function promptCancel() {
        divAlert.style.display = "none";
        if (divAlertCallback) {
            divAlertCallback(null);
            divAlertCallback = null;
        }
    }

    function confirmYes() {
        divAlert.style.display = "none";
        if (divAlertCallbackYes) {
            divAlertCallbackYes();
            divAlertCallbackYes = null;
        }
    }

    function confirmNo() {
        divAlert.style.display = "none";
        if (divAlertCallbackNo) {
            divAlertCallbackNo();
            divAlertCallbackNo = null;
        }
    }

    function alertOK() {
        divAlert.style.display = "none";
        if (divAlertCallback) {
            divAlertCallback();
        }
    }

    function prepare() {
        divAlert = document.createElement("div");
        divAlert.className = "alert";
        divAlert.style.display = "none";

        // eslint-disable-next-line require-unicode-regexp
        if ((/iPad|iPhone|iPod/g).test(navigator.userAgent)) {
            divAlert.classList.add("on-ipad");
        }

        divAlertContent = document.createElement("div");
        divAlertContent.className = "alert-content";
        divAlertContent.style.whiteSpace = "pre-wrap";

        divAlertInput = document.createElement("div");
        divAlertInput.className = "alert-prompt";

        alertInput = document.createElement("input");
        alertInput.type = "text";
        alertInput.onkeydown = function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                promptOK();
            }
        };

        divAlertInput.appendChild(alertInput);

        alertSelect = document.createElement("select");
        alertSelect.onchange = function () {
            alertInput.value = alertSelect.value;
        };

        divAlertInput.appendChild(alertSelect);

        const divAlertContentAndInput = document.createElement("div");
        divAlertContentAndInput.className = "alert-content-and-input";
        divAlertContentAndInput.appendChild(divAlertContent);
        divAlertContentAndInput.appendChild(divAlertInput);

        const divAlerPromptButton = document.createElement("div");
        divAlerPromptButton.className = "alert-prompt-buttons";
        divAlerPromptButton.appendChild(document.createElement("button"));
        divAlerPromptButton.lastChild.textContent = _("OK");
        divAlerPromptButton.lastChild.onclick = promptOK;
        divAlerPromptButton.appendChild(document.createElement("button"));
        divAlerPromptButton.lastChild.textContent = _("Cancel");
        divAlerPromptButton.lastChild.onclick = promptCancel;

        divAlertConfirm = document.createElement("div");
        divAlertConfirm.className = _("alert-confirm");
        divAlertConfirm.appendChild(document.createElement("button"));
        divAlertConfirm.lastChild.textContent = _("Yes");
        divAlertConfirm.lastChild.onclick = confirmYes;
        divAlertConfirm.appendChild(document.createElement("button"));
        divAlertConfirm.lastChild.textContent = _("No");
        divAlertConfirm.lastChild.onclick = confirmNo;

        divAlertButton = document.createElement("div");
        alertButtonOK = document.createElement("button");
        divAlertButton.appendChild(alertButtonOK);
        alertButtonOK.textContent = _("OK");
        alertButtonOK.onclick = alertOK;

        const divAlertOuter = document.createElement("div");
        divAlertOuter.className = "alert-outer";
        divAlertOuter.appendChild(divAlertContentAndInput);
        divAlertOuter.appendChild(divAlerPromptButton);
        divAlertOuter.appendChild(divAlertConfirm);
        divAlertOuter.appendChild(divAlertButton);
        divAlert.appendChild(divAlertOuter);
        document.body.appendChild(divAlert);
        document.body.addEventListener("keydown", function (e) {
            if (e.key === "Escape") {
                promptCancel();
                confirmNo();
            }
        });
    }

    global.myAlert = function (msg, callback) {
        if (!divAlert) {
            prepare();
        }

        divAlert.classList.remove("prompt");
        divAlert.classList.remove("choice");
        divAlert.classList.remove("choice-inline");

        divAlertContent.textContent = msg;
        divAlertInput.style.display = "none";
        divAlertConfirm.style.display = "none";
        divAlertButton.style.display = "";
        divAlertCallback = callback;
        divAlert.style.display = "";
        divAlertButton.getElementsByTagName("button")[0].focus();
    };

    global.myAlert.l10n = _;

    global.myPrompt = function (msg, callback, defaultText, options) {
        if (!divAlert) {
            prepare();
        }

        divAlert.classList.add("prompt");
        divAlert.classList.remove("choice");
        divAlert.classList.remove("choice-inline");

        divAlertContent.textContent = msg;
        divAlertInput.style.display = "";
        alertInput.style.display = "";
        alertInput.value = defaultText || "";
        alertInput.type = (options && options.type) || "text";
        alertSelect.style.display = "none";
        divAlertConfirm.style.display = "none";
        divAlertButton.style.display = "none";
        divAlertCallback = callback;
        divAlert.style.display = "";

        alertInput.focus();
        alertInput.setSelectionRange(0, alertInput.value.length);
        alertInput.select();
    };

    global.myConfirm = function (msg, callbackYes, callbackNo) {
        if (!divAlert) {
            prepare();
        }

        divAlert.classList.remove("prompt");
        divAlert.classList.remove("choice");
        divAlert.classList.remove("choice-inline");

        divAlertContent.textContent = msg;
        divAlertInput.style.display = "none";
        divAlertConfirm.style.display = "";
        divAlertButton.style.display = "none";
        divAlertCallbackYes = callbackYes;
        divAlertCallbackNo = callbackNo;
        divAlert.style.display = "";
        divAlertConfirm.getElementsByTagName("button")[0].focus();
    };

    global.myChoice = function (msg, options, callback, defaultValue) {
        if (!divAlert) {
            prepare();
        }

        divAlert.classList.remove("prompt");
        divAlert.classList.add("choice");

        if (options.dispositionInline) {
            divAlert.classList.add("choice-inline");
        } else {
            divAlert.classList.remove("choice-inline");
        }

        alertSelect.options.length = 0;
        for (const key of options.choices) {
            [].push.call(alertSelect.options, new Option(key, key));
        }
        alertSelect.value = alertInput.value = defaultValue;

        divAlertContent.textContent = msg;
        divAlertInput.style.display = "";
        alertInput.style.display = "none";
        alertSelect.style.display = "";
        divAlertConfirm.style.display = "none";
        divAlertButton.style.display = "none";
        divAlertCallback = callback;
        divAlert.style.display = "";
        divAlertConfirm.getElementsByTagName("button")[0].focus();
    };
}(this));
